---
title: IBIA report shows 31 abatement in suspicious wagering alerts
author: xforeal 
type: post
date: 2020-01-29T00:00:00+00:00
excerpt: 'Suspicious wagering cautions in 2019 fell by 31&amp;percnt; year-on-year, as indicated by a report from the International Betting Integrity Association (IBIA) '
url: /ibia-report-shows-31-abatement-in-suspicious-wagering-alerts/
image : images/news/2020/02/IBIA-1-1.jpg
categories:
  - news

---
Suspicious wagering cautions in 2019 fell by 31&percnt; year-on-year, as indicated by a report from the International Betting Integrity Association (IBIA).

The IBIA trustworthiness report found 183 alarms were accounted for to the important experts in 2019, 31&percnt; down on the 267 hailed in 2018.

Tennis saw the most huge drop of 43&percnt;, albeit still had the most alarms with 101; alongside football, it represented 82&percnt; of all cautions in 2019, with 48&percnt; originating from Europe.

The report discovered 12 distinct games across five landmasses had alarms detailed, with 52&percnt; outside Europe speaking to a 7&percnt; yearly increase.

IBIA CEO Khalid Ali stated: &#171;The decrease in alarms is exceptionally welcome, particularly as this is principally a consequence of an improved degree of respectability in ITF tennis, which has been the subject of specific investigation as of late. In any case, there stays an unmistakable danger from lawbreakers aim on controlling game to cheat operators.

&#171;We keep on working intimately with sports and our individuals to lessen that danger and to distinguish and rebuff such defilement, using the world&rsquo;s biggest administrator run and client information drove honesty system.

&#171;Our rebranding and worldwide repositioning in 2019 has helped our extension, with administrators progressively perceiving the worth and business need of taking part in aggregate activity to secure their items against the loss of income coming about because of wagering corruption.&#187;