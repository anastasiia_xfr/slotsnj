---
title: Betway broadens Kevin Pietersen s diplomat part until 2023
author: xforeal 
type: post
date: 2020-09-18T00:00:00+00:00
excerpt: 'Betway has broadened its organization with previous England global cricketer Kevin Pietersen until 2023 '
url: /betway-broadens-kevin-pietersen-s-diplomat-part-until-2023/
image : images/news/2020/09/betway-1.jpg
categories:
  - news

---
Betway has expanded its organization with previous England worldwide cricketer Kevin Pietersen until 2023. 

The currently resigned batsman will proceed in his part as legitimate worldwide cricket and brand envoy for the administrator. 

The job will incorporate Pietersen imparting his knowledge and insight on world cricket through substance on Betways insider blog, just as in the background access. 

Initially the previous batsman, who last played for England in 2014 and is at present the countrys fifth driving unequaled Test coordinate run scorer, will zero in on giving week after week updates and expectations on the Indian Premier League (IPL) which starts on 19 September. 

Betway promoting and activities chief Paul Adkins, stated: Kevin enhances our cricket offering and Betway clients over the world have made the most of his exceptional and legit knowledge. 

The cricket pages of our selective Betway Insider blog will keep on being full to the edge with extraordinary substance from one of the most gifted parts in the games history. 

The arrangement proceeds Betways center around the game, subsequent to expanding its authority wagering association with Cricket West Indies (CWI) prior this week, which incorporates sponsorship of the apparent multitude of West Indies people home worldwide matches until 2022. 

While in July, the administrator marked a three-year game plan with Cricket South Africa (CSA), as lead backer of Test and One Day International (ODI) arrangement&#8217;s in the nation.