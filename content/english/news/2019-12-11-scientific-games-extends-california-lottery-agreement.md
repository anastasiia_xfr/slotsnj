---
title: Scientific Games extends California Lottery agreement
author: xforeal 
type: post
date: 2019-12-11T00:00:00+00:00
excerpt: Scientific Games has announced a three-year extension to its agreement with the California State Lottery
url: /scientific-games-extends-california-lottery-agreement/
image : images/news/2019/12/scientificgamesrevenue.jpg
categories:
  - news

---
Scientific Games has announced a three-year extension to its agreement with the California State Lottery.

The new deal will see the supplier continue as provider of the Lottery&rsquo;s instant games and associated services through November 2022, with the option to extend the contract until 2025.

Scientfic Games has supplied the Lottery with instant games nearly every year since its inception in 1985.

John Schulz, SVP, Lottery Instant Products for Scientific Games, said: &#8220;As the California Lottery&#8217;s profits grow responsibly, so does the collective impact our partnership has on public education in the state.

&#8220;We value the trust the California Lottery has placed in Scientic Games, and we are proud to continue our focus on innovation and growth that ultimately benefits students in the state of California.&rdquo;

Scientific Games holds a number of lottery contracts, including signing a deal with the Turkish Lottery in September this year.

The supplier generated revenue of $855m for Q3 2019, up 4% year-on-year.

Adjusted EBITDA was $344m, an increase of 6%, while net income was $18m, a significant increase from the $352m loss of the prior year period.