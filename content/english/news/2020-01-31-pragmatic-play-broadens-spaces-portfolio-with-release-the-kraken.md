---
title: Pragmatic Play broadens spaces portfolio with Release the Kraken
author: xforeal 
type: post
date: 2020-01-31T00:00:00+00:00
excerpt: 'Pragmatic Play, a main substance supplier for the gaming business, has propelled another element filled video opening, Release the Kraken '
url: /pragmatic-play-broadens-spaces-portfolio-with-release-the-kraken/
categories:
  - news

---
Pragmatic Play, a main substance supplier for the gaming business, has propelled another element filled video opening, _Release the Kraken_.

The 4&#215;5, realistic rich title is set in the profundities of the sea where players are hollowed in a fight against the frightening animal of the profound, the Kraken.

The Sunken Treasure Bonus will trigger an entirety of multipliers, while the Roaming Kraken Free Spins raises 10 things among which free twists are covered up. All the uncovered free twists will be included as the player finds them. The Kraken modifier highlights incorporate Infectious Kraken Wilds, Colossal Kraken Wilds and Kraken Locking Wilds, bringing about various sorts of win combinations.

Melissa Summerfield, Chief Commercial Officer at Pragmatic Play, stated: &ldquo;_Release the Kraken_ is the fourth game we&rsquo;ve propelled for the current month, speaking to one more valuable expansion to the Pragmatic Play opening portfolio.

&ldquo;With its vivid setting, exciting ongoing interaction and monster win potential, we are certain players will fight the fearsome Kraken looking for its treasure.&rdquo;

_Release the Kraken_ is the most current expansion to Pragmatic Play&rsquo;s grant winning assortment of opening games which incorporates _Money Mouse, Mustang Gold_ and fan-most loved _Wolf Gold._ 

The provider&rsquo;s whole arrangement of video spaces, bingo, and different games is accessible to its administrator accomplices by means of _one_ single API.