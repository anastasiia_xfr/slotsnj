---
title: NetEnt packages Red Tiger games with NetEnt Connect following acquisition
author: xforeal 
type: post
date: 2019-12-09T00:00:00+00:00
excerpt: NetEnt has integrated its games with those from the recently acquired Red Tiger Gaming, with the launch of the NetEnt Connect platform
url: /netent-packages-red-tiger-games-with-netent-connect-following-acquisition/
image : images/news/2019/12/NetEnt.jpg
categories:
  - news

---
NetEnt has integrated its games with those from the recently acquired Red Tiger Gaming, with the launch of the NetEnt Connect platform.

The platform has been developed in a closed beta, with a wider release planned for early next year, and Red Tiger is the first games supplier to be included in the service, after it was puchased by NetEnt in September for an initial &pound;197m ($243.1m).&nbsp;

The acquisition was received well, with the share price of the Swedish company rising from SEK 25.60 ($2.63) to SEK 33.00 the week following the announcement.

However, this was not enough to prevent a 1% decrease year-on-year for its Q3 revenue, but that included approximately SEK 55m ($5.7m) of transactions and financing-related costs from the Red Tiger deal.

Henrik Fagerlund, Managing Director of NetEnt Malta and Chief Product Officer, said: &ldquo;The speed with which we have been able to go live with Red Tiger content on some of NetEnt customers bears testament to the hard work and collaboration since the acquisition was completed. It also shows great promise for NetEnt Connect which is a key part of NetEnt strategy going forward.&rdquo;