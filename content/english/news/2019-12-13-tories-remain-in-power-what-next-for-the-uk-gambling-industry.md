---
title: 'Tories remain in power  What next for the UK gambling industry'
author: xforeal 
type: post
date: 2019-12-13T00:00:00+00:00
excerpt: The Conservatives eased to a very decisive victory in the General Election yesterday, comfortably winning 364 seats at the time of writing, with Labour a distant second with 203 seats
url: /tories-remain-in-power-what-next-for-the-uk-gambling-industry/
image : images/news/2019/12/Parliament.jpg
categories:
  - news

---
The Conservatives eased to a very decisive victory in the General Election yesterday, comfortably winning 364 seats at the time of writing, with Labour a distant second with 203 seats.

The bones of why and how such a dramatically large victory came about will no doubt be analysed in every spare column inch in the coming days, but what does this mean for the UK gambling industry?

With Brexit dominating the headlines for much of the election, and indeed the past three- and-a-half years, it is perhaps not surprising gambling was not top of the agenda. Boris Johnson&rsquo;s party first references the subject 11 pages into its manifesto with a pledge that: &ldquo;We will continue to take action to tackle gambling addiction.&rdquo;

The party then later refers to a desire to legislate to make the UK the safest place to be online, before later saying in the same paragraph: &ldquo;Also, given how the online world is moving, the Gambling Act is increasingly becoming an analogue law in a digital age.&nbsp;We will review it, with a particular focus on tackling issues around loot boxes and credit card misuse.&rdquo;

Exactly what it means by referring to it as an analogue law in a digital age I will leave to the aforementioned column inches, but the Conservative Party does offer a clear sense of direction, in terms of where things may go.

Targeting gambling addiction ought to be a positive step, but how that aim manifests itself, in terms of the effect on the industry, remains to be seen. If it targets increased treatment for those with problem gambling, it will be a positive step for the image of the industry, yet if it turns into imposing restrictions, then it could be problematic.

It is unsurprising to see loot boxes enter the political equation, with controversy having been stoked numerous times in the video gaming industry in the past two years. It essentially refers to when people pay money for a random assortment of items within a certain range within a video game, but without any specific item being guaranteed.

While they do not currently fall under the UK definition of gambling, it would not be surprising to see these restricted further, whether through a minimum age limit being introduced, or a requirement to change how they work in general.

The biggest sign of where the Conservative Party may go actually comes from a release on its website on 28 November, however. In this, it pledged a new UK-wide cross-government addiction strategy which will be published in 2020.

The party said it will commission an independent review into the 2005 Gambling Act. The review will be making recommendations on: prize and stake limits; the misuse of credit card payments (which was already being looked at by the Gambling Commission), putting the voluntary levy on a statutory footing and new ways of raising revenue for problem gambling support.&nbsp;

The voluntary levy on a statutory footing means little. While it will be written into law, it will be voluntary, so could change very little. However, earlier this year, owners of several large gambling companies offered to up the levy, which earned &pound;10m last year, from 0.1% to 1% across the next five years.

The new ways of raising revenue for problem gambling support will depend on exactly where it comes from and, along with the prize and stake limits, it would be mere speculation at this stage as to how it materialises.

Yet the fact the release quotes Matt Hancock, the Health Secretary, may provide a twinge of concern to the business. I fail to see how gambling being discussed under health, as opposed to them quoting the Department for Culture Media and Sport minister Nigel Adams, can be reassuring. This is likely more due to the statement addressing other health issues however and it is perhaps harsh to read too much into it.

The release also mentioned the raising of the number of NHS gambling clinics from two to 14, which can only be a good thing. In essence, and somewhat inevitably, it is a case of wait and see. The main impacts from what the Conservative Party is saying will boil down to exactly what it means, with a lack of exact detail leaving it difficult to predict.&nbsp;

Had Labour won, it pledged to treat it as a matter of public health, something the Conservative Party seems to imply it will do.&nbsp;Labour also pledged to curb gambling advertising in sports even further, following the pre-watershed ban on in-play advertising during sporting events that came in this football season, and similarly address the Gambling Act by bringing in a new, reformed act, which given how far-reaching that could prove to be, would have been of serious concern to the industry.

I don&rsquo;t think the first would have been eventually popular. Yes, the public might greet it well, but it could have a knock-on effect on other things. What if there was eventually less money available for the operators to put into sport via sponsorship, for example? Given the popularity of the sport and the ubiquitous presence of gambling adverts in sports coverage, then I cannot see how the industry would react positively to it.&nbsp;

The party also said it would have established gambling limits, a levy for problem gambling and funding and mechanisms for consumer compensations. In essence, this would mean a lot of change, which in the current climate, I could not see&nbsp; going down well at all.

It seems incredibly unlikely, now that Johnson has a clear majority, that anything will stop the country from leaving the EU shortly. That will be the question, whereas getting concerned at this stage for &nbsp;statements about gambling during the election would feel somewhat pre-emptive, it is a debate to be had &nbsp;at a later point.

Once the dust settles after the election, the intentions will hopefully become clearer, but it is difficult to see gambling becoming a top priority given the current climate, so we might be waiting a while.

&nbsp;

&nbsp;