---
title: CEO savors colossal honor as Football Index accomplishes Sunday Times accolade
author: xforeal 
type: post
date: 2020-09-15T00:00:00+00:00
excerpt: |
  |
    Football Index has positioned second in The Sunday Times'Sage Tech Track 100, which CEO Adam Cole has depicted as a "gigantic honour
url: /ceo-savors-colossal-honor-as-football-index-accomplishes-sunday-times-accolade/
image : images/news/2020/09/FootballIndex-4.jpg
categories:
  - news

---
Football Index has positioned second in The Sunday Times&#8217;Sage Tech Track 100, which CEO Adam Cole has depicted as a &#171;gigantic honour.&#187; 

The honor is an amazing one, considering the site was dispatched in October 2015. 

It has more than 500,000 clients, its own digital broadcast and a YouTube channel, permitting players to purchase and sell partakes in footballers. 

The administrator works in a manner that permits Football Index to take commission off each exchange, instead of a player versus bookmaker situation. 

Adam Cole, Football Index CEO, stated: This is a colossal honor for us that perceives the splendid development of Football Index, and the item weve created. 

&#171;Given the UKs amazing tech part, moving to runner up is a fantastic outcome, setting up us as one of the UKs most noteworthy fintech examples of overcoming adversity.