---
title: Newgioco Announces Listing of Shares on The Nasdaq Stock Exchange
author: xforeal 
type: post
date: 2019-12-31T00:00:00+00:00
excerpt: Newgioco Group, Inc
url: /newgioco-announces-listing-of-shares-on-the-nasdaq-stock-exchange/
image : images/news/2019/12/newgioco.jpg
categories:
  - news

---
<p class="canvas-iota canvas-content Mb(1.0em) Mb(0)- - sm Mt(0.8em)- - sm" style="margin: 0px 0 10px 0; shading: #26282a; textual style family: 'Helvetica Neue', Helvetica, Arial, sans-serif; text dimension: 15px; text style: typical; textual style variation ligatures: ordinary; text style variation tops: typical; text style weight: 400; letter-dividing: typical; vagrants: 2; content adjust: start; content indent: 0px; content change: none; blank area: ordinary; widows: 2; word-dispersing: 0px; - webkit-content stroke-width: 0px; foundation shading: #ffffff; content embellishment style: introductory; content adornment shading: beginning;" information="" reactid="11">
  Newgioco Group, Inc. (“Newgioco” or the “Organization”) (<a href="https://finance.yahoo.com/q?s=nwgi">NWGI</a>), a worldwide games wagering and gaming innovation organization giving completely incorporated programming answers for on the web and land-based games wagering and relaxation gaming administrators, is satisfied to report that its regular stock will start exchanging on the NASDAQ Capital Market successful with the opening of exchanging on December 27, 2019. The Company’s basic stock will keep on exchanging under the image “NWGID” until January 13, 2020 after which the image will be “NWGI”.
</p>

<p class="canvas-particle canvas-content Mb(1.0em) Mb(0)- - sm Mt(0.8em)- - sm" information="" reactid="12">
  The Company is one of the pioneers in the firmly directed Italian recreation wagering market, working a strong retail system of roughly 2,300 wagering areas all through Italy, and claims a profoundly separated restrictive wagering innovation stage casually known as Elys Gameboard (“Elys”). Elys, developed starting from the earliest stage the most recent Microsoft.Net Core structure, is a profoundly adaptable and customisable omnichannel sportsbook motor intended to adapt to the requests of the present wagering administrators and players.
</p>

<p class="canvas-molecule canvas-content Mb(1.0em) Mb(0)- - sm Mt(0.8em)- - sm" information="" reactid="12">
  The Elys engineering permits the executives of wager danger of every exchange through on the web (PC and versatile) channels and at each land-based area from which a wager is set and is expected to address the autonomous administrator’s capacity to adequately go up against bigger and increasingly settled establishment administrators. With the nullification of the Professional and Amateur Sports Protection Act of 1992, the Company accepts the United States showcase speaks to a huge addressable market open door for its Elys wagering platform.
</p>

<p class="canvas-iota canvas-content Mb(1.0em) Mb(0)- - sm Mt(0.8em)- - sm" information="" reactid="13">
  “The posting of our basic stock on the Nasdaq is a significant achievement for the organization and the consequence of around twenty years of devoted business improvement in the controlled relaxation wagering industry. We accept that posting on the Nasdaq ought to widen our investor base by drawing in new financial specialists, improve Newgioco’s perceivability in the commercial center and liquidity of our stock, and at last, form long haul investor esteem,” expressed Michele (Mike) Ciavarella, Newgioco Chief Executive Officer.
</p>

<p class="canvas-molecule canvas-content Mb(1.0em) Mb(0)- - sm Mt(0.8em)- - sm" information="" reactid="13">
  “I might want to stretch out our ardent thankfulness to our lawful groups at Gracin & Marlow, LLP and Beard Winter, LLP, that got behind our submitted administration and current board, for genuinely making Newgioco an individual from this esteemed, and comprehensively confided in stock exchange.”
</p>