---
title: 'Analysis  The National Lottery     time for change'
author: xforeal 
type: post
date: 2019-12-10T00:00:00+00:00
excerpt: 'Since the UK National Lottery was created in 1994, it has raised over &pound;40bn ($52'
url: /analysis-the-national-lottery-time-for-change/
image : images/news/2019/12/National-lottery.jpg
categories:
  - news

---
Since the UK National Lottery was created in 1994, it has raised over &pound;40bn ($52.7bn) for its chosen causes, paying out over &pound;59bn in prizes.

A vital source of funding for UK causes, National Lottery revenue contributed almost &pound;2.2bn towards the cost of staging the 2012 Olympic and Paralympic games in London, and continues to fund thousands of projects in health, education, sport and arts.

Canadian-owned operator Camelot has run the UK National Lottery since its inception, winning all three of the licenses offered in 1994, 2001 and finally 2009 &#8211; which was then extended in 2012.

Camelot&rsquo;s latest agreement expires in 2023 and bidding is due to formally launch in early 2020 for the lottery&rsquo;s fourth license, with the chosen operator to be announced next year.

It goes without saying that with such a profitable and well-known lottery on the table, there are several interested parties; most recently it was reported French operator Fran&ccedil;aise des Jeux (FDJ) plans to make a bid.

This raises the question of whether the National Lottery is better left in the hands of the experienced Camelot, or whether it would benefit from a new operator with fresh ideas.

**Camelot: The case for continuity**

When contacted by _Gambling Insider_, a Camelot spokesperson made clear the operator was interested in making a bid for the fourth license.

The spokesperson explained how the lottery is &#8220;in its best ever shape&#8221; under Camelot control, but while this might be true in 2019, its performance has not been completely consistent in recent years.

National Lottery ticket sales fell 9% year-on-year for 2016/17, to &pound;6.92bn, causing Camelot to review its strategy for the following years. The operator attributed this mainly to competition from secondary lottery products, such as Lottoland.

Despite this slump, Camelot&rsquo;s strategic review seems to have been effective, with ticket sales increasing yearly from this point and digital sales growing at a record pace.

This year, Camelot announced ticket sales were up 14% to &pound;3.92bn for H1, while digital sales grew by 40% and retail sales by 5%.

Evidently, the operator&rsquo;s progress has been positive since 2017, but whether its bid will be successful is another story; particularly considering the operator&rsquo;s turbulent relationship with the Gambling Commission (GC) during the current contract.

In 2012, Camelot issued judicial proceedings against the GC after it claimed the regulatory body had failed to take appropriate action against the newly formed Health Lottery, which the operator said undermined its own operations.

More recently, in August last year, Camelot was fined &pound;1.2m by the GC for five controls-related failures. Although the lottery&rsquo;s results have been improving in recent years, the GC might prefer to start afresh with a new operator with which it can share a better relationship.

**FDJ &#8211; The case for change**

Reports suggest FDJ has been in discussions with Rothschild, the investment bank leading the search for the new National Lottery operator.

The operator was privatised this year after the French Assembly voted to allow French President Emmanuel Macron&rsquo;s En March to sell its 72% share in the company.

FDJ&rsquo;s results have been strong since the privatisation, reporting a revenue increase of 7% year-on-year to &euro;1.4bn ($1.84bn) during the first nine months of 2019.

In addition, lottery is already an important segment for FDJ, with Euromillions and Lotto accounting for 20% of the operator&rsquo;s total stakes for the same period, rising 6% to &euro;10bn.

Undoubtedly, privatisation has boosted the operator&rsquo;s profits and the experience of already running lotteries could swing in FDJ&rsquo;s favour.

However, the freshness of the privatisation could play a part in deterring the GC, with no evidence of the operator&rsquo;s long-term results while outside of state control.

**Other candidates**

Other rumoured bidders for the National Lottery tender include Virgin Group founder Sir Richard Branson and Northern and Shell Media Group owner Richard Desmond.

Desmond boasts lottery experience, as owner of the Health Lottery &ndash; the cause of Camelot&rsquo;s 2012 court dispute.

Camelot ultimately lost the High Court action to block the rival lottery and the Health Lottery is still running today, raising over &pound;100m so far for UK health services.

Desmond&rsquo;s lottery experience could prove vital in the contest for the 2023 contract and a UK-based operator might be favoured in comparison to FDJ.

Branson made failed bids for the National Lottery in both 1994 and 2000 and it is rumoured he is considering a third attempt.

Other parties reportedly in discussions for the tender include Dutch company Novamedia, operator of the People&rsquo;s Postcode Lottery and Czech operator Sazka.