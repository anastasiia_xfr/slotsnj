---
title: Cowlitz Indian Tribe first Washington clan to acknowledge club smaller amendment
author: xforeal 
type: post
date: 2020-09-15T00:00:00+00:00
excerpt: 'The Cowlitz Indian Tribe is the primary clan in Washington to go to an arrangement for the change of its Class III gaming conservative, including a few new provisions '
url: /cowlitz-indian-tribe-first-washington-clan-to-acknowledge-club-smaller-amendment/
image : images/news/2020/09/casinochips-2.jpg
categories:
  - news

---
The Cowlitz Indian Tribe is the main clan in Washington to go to an arrangement for the revision of its Class III gaming minimal, including a few new arrangements. 

The Washington State Gambling Commission (WSGC) has affirmed the new conditional concurrence with the clan, which works the Ilani Casino Resort in Ridgefield, Washington. 

As aspect of this alteration, the clan will be needed to give extra subsidizing to the network, including beneficent gifts. 

The clan will likewise be needed to make and keep up a mindful betting system and give financing to issue betting treatment. 

The minimal considers the activity of 125 gaming tables in a single gaming office or a blend of two offices, and grants 25&percnt; of table games to present to $1,000 bets with restricted tables offering $5,000 after client screening. 

Commenting on the arrangement, clan executive Philip Harju, stated: for the Cowlitz Indian Tribe, we might want to thank the Washington State Gambling Commission, the Washington State Legislature and the Governors Office for their proceeded with help on this undertaking, yet additionally the endeavors that made ready to Ilanis opening. 

The conservative should at present experience various legislative strides before it is marked into law, including a WSGC vote on 15 Oct and last thought by the state Governor.