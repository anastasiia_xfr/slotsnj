---
title: Betway expands Kevin Pietersen s envoy function until 2023
author: xforeal 
type: post
date: 2020-09-18T00:00:00+00:00
excerpt: 'Betway has broadened its organization with previous England global cricketer Kevin Pietersen until 2023 '
url: /betway-expands-kevin-pietersen-s-envoy-function-until-2023/
image : images/news/2020/09/betway-2.jpg
categories:
  - news

---
Betway has expanded its organization with previous England worldwide cricketer Kevin Pietersen until 2023. 

The presently resigned batsman will proceed in his function as authentic worldwide cricket and brand diplomat for the administrator. 

The job will incorporate Pietersen imparting his understanding and insight on world cricket through substance on Betways insider blog, just as in the background access. 

Initially the previous batsman, who last played for England in 2014 and is presently the countrys fifth driving untouched Test coordinate run scorer, will zero in on giving week after week updates and forecasts on the Indian Premier League (IPL) which starts on 19 September. 

Betway advertising and tasks chief Paul Adkins, stated: Kevin enhances our cricket offering and Betway clients over the world have made the most of his extraordinary and legitimate understanding. 

The cricket pages of our select Betway Insider blog will keep on being full to the edge with incredible substance from one of the most capable parts in the games history. 

The arrangement proceeds Betways center around the game, subsequent to expanding its authority wagering association with Cricket West Indies (CWI) prior this week, which incorporates sponsorship of the apparent multitude of West Indies people home global matches until 2022. 

While in July, the administrator marked a three-year game plan with Cricket South Africa (CSA), as lead backer of Test and One Day International (ODI) arrangement&#8217;s in the nation.