---
title: Okada s offer against Universal Entertainment dismissed
author: xforeal 
type: post
date: 2020-09-21T00:00:00+00:00
excerpt: 'Universal Entertainment Corporation has given an announcement affirming the Tokyo High Court has excused an allure from Kazuo Okada, the companys founder '
url: /okada-s-offer-against-universal-entertainment-dismissed/
image : images/news/2020/09/okadainvestigation-1.jpg
categories:
  - news

---
Universal Entertainment Corporation has given an announcement affirming the Tokyo High Court has excused an allure from Kazuo Okada, the companys originator. 

Previously, the Tokyo District Courtordered Okada to pay JPY21.3m ($193,000) in harms for three separate instances of deceitful acts. 

In the announcement, the organization said the courts perceived the deceitful demonstrations were led under Okada and furthermore recognized that Mr Okada penetrated the two his obligation of care of a decent supervisor and his guardian obligation of reliability as a head of the organization, and acknowledged all cases of the organization. 

The Tokyo High Court not just maintained the choice from the District Court, as indicated by Inside Asian Gaming, Okada will likewise need to pay enthusiasm of 5&percnt; per annum predated to 29 December 2017 or more all case costs. 

Universal and its auxiliary Tiger Resort Asia looked for harms from Okada in light of the fact that he supposedly moved organization assets to individual records. 

Okada was eliminated as Universal director in June 2017, and the main case was recorded in November of the exact year. 

The three cases are worked around a HKD135mloan that Tiger Resort allowed to an outsider, HKD16m that was procured from Tiger Resort without authorisation and acquisition of land implied for an IR for individual addition.