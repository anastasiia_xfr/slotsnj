---
title: Cowlitz Indian Tribe first Washington clan to acknowledge gambling club minimized amendment
author: xforeal 
type: post
date: 2020-09-15T00:00:00+00:00
excerpt: 'The Cowlitz Indian Tribe is the primary clan in Washington to go to an arrangement for the alteration of its Class III gaming smaller, including a few new provisions '
url: /cowlitz-indian-tribe-first-washington-clan-to-acknowledge-gambling-club-minimized-amendment/
image : images/news/2020/09/casinochips-1.jpg
categories:
  - news

---
The Cowlitz Indian Tribe is the main clan in Washington to go to an understanding for the correction of its Class III gaming conservative, including a few new arrangements. 

The Washington State Gambling Commission (WSGC) has affirmed the new speculative concurrence with the clan, which works the Ilani Casino Resort in Ridgefield, Washington. 

As aspect of this change, the clan will be needed to give extra financing to the network, including altruistic gifts. 

The clan will likewise be needed to make and keep up a mindful betting system and give subsidizing to issue betting treatment. 

The minimal takes into consideration the activity of 125 gaming tables in a single gaming office or a mix of two offices, and grants 25&percnt; of table games to present to $1,000 bets with restricted tables offering $5,000 after client screening. 

Commenting on the understanding, clan executive Philip Harju, stated: for the benefit of the Cowlitz Indian Tribe, we might want to thank the Washington State Gambling Commission, the Washington State Legislature and the Governors Office for their proceeded with help on this undertaking, yet in addition the endeavors that prepared to Ilanis opening. 

The smaller should in any case experience various legislative strides before it is marked into law, including a WSGC vote on 15 Oct and last thought by the state Governor.