---
title: EveryMatrix set to enter Spain with CasinoEngine
author: xforeal 
type: post
date: 2019-12-12T00:00:00+00:00
excerpt: Supplier EveryMatrix is set to enter the Spanish market after the successful technical assessment of its CasinoEngine product
url: /everymatrix-set-to-enter-spain-with-casinoengine/
image : images/news/2019/12/CasinoEngineSpain.jpg
categories:
  - news

---
Supplier EveryMatrix is set to enter the Spanish market after the successful technical assessment of its CasinoEngine product.

Passing the assessment means CasinoEngine is fully certified after complying with technical, security and vulnerability standards imposed by the Spanish gambling regulator &ndash; the &nbsp;Direcci&oacute;n General de Ordenaci&oacute;n del Juego (DGOJ).

CasinoEngine focuses on content aggregation and provides services to sports betting and casino operators, such as Tipico, Wunderino and Mybet.

The news strengthens EveryMatrix&rsquo; foothold in Europe, as it now holds software provider licenses in Malta, the UK, Denmark, Romania and Curacao, whereas CasinoEurope is compliant in Sweden, Norway and now Spain, among others.

Stian Hornsletten, Group CCO at EveryMatrix, said:&nbsp;&ldquo;Helping casino operators to enter new jurisdictions is a key objective of our long-term plans.

&ldquo;We are happy to be setting our footprint into new regulated markets and prove that our technology can be effectively adapted to meet specific regulatory requirements.&#8221;

The news comes days after EveryMatrix expanded its portfolio with the release of RGS Matrix, a remote gaming server that aims to enhance player experience and deliver faster content.