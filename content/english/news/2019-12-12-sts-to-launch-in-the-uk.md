---
title: STS to launch in the UK
author: xforeal 
type: post
date: 2019-12-12T00:00:00+00:00
excerpt: STS has obtained a license from the Gambling Commission, which will make it the first Polish bookmaker to operate in the UK market
url: /sts-to-launch-in-the-uk/
image : images/news/2019/12/stsgreep.jpg
categories:
  - news

---
STS has obtained a license from the Gambling Commission, which will make it the first Polish bookmaker to operate in the UK market.

The gaming group is active in more than a dozen European countries, and will be the first Polish bookmaker to operate outside Poland.

STS CEO Mateusz Juroszek said: &ldquo;Obtaining a license in the United Kingdom is the result of a consistently implemented international expansion strategy. We are always looking at other prospective foreign markets.&#8221;

Juroszek spoke to _Trafficology_ last month, explaining plans to expand into the international market which started in February, referencing barriers the Polish Government were putting in place for sports betting operators.

This includes strict advertising regulations and focusing on getting &#8220;as much tax as possible,&#8221; rather than responsible gambling as seen in the UK, but is still keen to improve the market in Poland.

The news follows last week&rsquo;s announcement of STS&rsquo; partnership with online gaming conversion platform Enteractive&rsquo;s reactivation cloud, aimed at improving player retention.