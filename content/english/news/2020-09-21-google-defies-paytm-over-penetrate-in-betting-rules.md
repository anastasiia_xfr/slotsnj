---
title: Google defies Paytm over penetrate in betting rules
author: xforeal 
type: post
date: 2020-09-21T00:00:00+00:00
excerpt: 'One of Indias biggest online installment processors, Paytm, was briefly taken out from the Google Play Store over purportedly defying Google guidelines against the help of gambling '
url: /google-defies-paytm-over-penetrate-in-betting-rules/
image : images/news/2020/09/Google-1.jpg
categories:
  - news

---
One of Indias biggest online installment processors, Paytm, was briefly taken out from the Google Play Store over purportedly defying Google guidelines against the help of betting. 

Paytm ran an advancement where clients could get stickers for each exchange, which can later be reclaimed for Paytm Cashback. The advancement concurred with the beginning of the Indian Premier League T20 competition. 

However, Google delivered an announcement saying it wont permit any applications in the store that penetrate its no-betting arrangements, or lead clients to an outer site that permits them to partake in paid competitions to win genuine cash or money prizes. 

Paytm educated its clients: &#171;We got correspondence from Google that they are suspending our application since they accept this to be an infringement of their Play Store strategies on betting. 

&#171;The Paytm Android application has in this manner been unlisted from Googles Play Store and is briefly inaccessible to clients for new downloads or updates. 

The application rushed to eliminate the advancement and is currently accessible on the store again. 

But Paytm keeps up the cashback crusade was inside the rules. The organization included: We wish to clarify that driving traffic or advancing dream sports isn&#8217;t betting! 

Paytm likewise demanded Google is making approaches which are well beyond the laws of our nation, and are subjectively actualizing them.