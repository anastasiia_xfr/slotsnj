---
title: Macau Legend Development Ltd ends exchanging shares
author: xforeal 
type: post
date: 2020-09-15T00:00:00+00:00
excerpt: 'Macau Legend Development Limited declared it will be stopping exchanging the portions of the Stock Exchange of Hong Kong Limited from 9am on September 14, 2020 '
url: /macau-legend-development-ltd-ends-exchanging-shares/
image : images/news/2020/09/macau-1.jpg
categories:
  - news

---
<span data-contrast="auto">Macau Legend Development Limited declared it will be stopping exchanging the portions of the Stock Exchange of Hong Kong Limited from 9am on September 14, 2020. The exchanging end was actualized forthcoming the arrival of a declaration of inside data of the Company compliant with the Hong Kong Code on Takeovers and Mergers. The organization </span><span data-contrast="auto">didnt </span><span data-contrast="auto">provide extra data right now. </span><span data-ccp-props='{"134233117":true,"134233118":true,"201341983":0,"335559739":200,"335559740":240}' />

<span data-contrast="auto">Macau Legend works the Macau Fishermans Wharf and the </span><span data-contrast="auto">Savan </span><span data-contrast="auto">Legend Resort in Laos, and as per </span><span data-contrast="auto">GGRAsia </span><span data-contrast="auto">, there host been gatherings that are keen on procuring the Fishermans Wharf scene. In August, Macau Legends CEO Melinda Chan Mei Yi guaranteed the organization isn&#8217;t effectively hoping to sell the setting yet affirmed the intrigue is there. Macau Legend revealed a HKD550.2m ($71m) misfortune for H1 2020, contrasted with HKD107.5m for a similar period the earlier year. </span><span data-ccp-props='{"134233117":true,"134233118":true,"201341983":0,"335559739":200,"335559740":240}' />

<span data-contrast="auto">Macau Legend isn&#8217;t the main organization to end its exchanging. Get Nice Holdings gave a comparably worded notice, expressing, exchanging the portions of the Company on The Stock Exchange of Hong Kong Limited will be stopped with impact from 9:00 a.m. on 14 September 2020 forthcoming the arrival of a declaration comparable to the Hong Kong Code on Takeovers and Mergers, which is inside data of the Company. Get Nice used to possess Waldo club in Macau and is presently planning for an offer for Nagasakis IR. </span><span data-ccp-props='{"134233117":true,"134233118":true,"201341983":0,"335559739":200,"335559740":240}' />