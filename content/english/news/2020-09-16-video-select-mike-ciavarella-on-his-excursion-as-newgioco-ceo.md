---
title: Video select Mike Ciavarella on his excursion as Newgioco CEO
author: xforeal 
type: post
date: 2020-09-16T00:00:00+00:00
excerpt: "Newgioco executive and CEO Mike Ciavarella joins NJ Slots Online for the most recent scene of the GI Huddle, talking through his profession in gaming and Newgioco's excursion to date "
url: /video-select-mike-ciavarella-on-his-excursion-as-newgioco-ceo/
image : images/news/2020/09/mikeyciavarella-3.jpg
categories:
  - news

---
Newgioco executive and CEO Mike Ciavarella joins _NJ Slots Online_ for the most recent scene of the _GI Huddle_ , talking through his profession in gaming and Newgioco&#8217;s excursion to date. 

Having appreciated accomplishment in Italy and having ventured into the US, Ciavarella examines Newgioco&#8217;s omni-channel administrations, just as zeroing in on the organization&#8217;s games wagering stage Elys. 

On head of the specialized subtleties, in any case, the CEO additionally discusses his past as a hockey mentor and in the fund area. 

Ciavarella will highlight on the front of _NJ Slots Online_ &#8216;s&#187; forthcoming &#187; _Sports Betting Focus_ magazine, so make certain to peruse the full meeting when it is distributed. 

Below, hit play to see the video rendition, as the Newgioco CEO talks about his profession up until now, and tentative arrangements, for the _GI Huddle_ . 

<div class="videoWrapper">
  <iframe loading="lazy" allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/CoqgxeOjxkI" width="560" />
</div>