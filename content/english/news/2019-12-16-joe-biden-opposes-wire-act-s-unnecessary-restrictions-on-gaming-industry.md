---
title: 'Joe Biden opposes Wire Act   s    unnecessary restrictions    on gaming industry'
author: xforeal 
type: post
date: 2019-12-16T00:00:00+00:00
excerpt: 'Joe Biden says he is opposed to &#8220;unnecessary restrictions&#8221; on the gaming industry, ahead of the Department of Justice&rsquo;s (DoJ) deadline to appeal a new New Hampshire ruling of its Federal Wire Act reinterpretation, which ends on Friday'
url: /joe-biden-opposes-wire-act-s-unnecessary-restrictions-on-gaming-industry/
image : images/news/2019/12/DepartmentOfJustice.jpg
categories:
  - news

---
Joe Biden says he is opposed to &#8220;unnecessary restrictions&#8221; on the gaming industry, ahead of the Department of Justice&rsquo;s (DoJ) deadline to appeal a new New Hampshire ruling of its Federal Wire Act reinterpretation, which ends on Friday.

The DoJ &nbsp;issued an opinion last year which stated the law was applicable to all forms of gambling &ndash; including online casino, poker and lottery &ndash; not just sports betting.

However, following a New Hampshire lawsuit earlier this year, the opinion was reversed, deeming the Act to only apply to sports betting, although the DoJ is readying an appeal.

Biden, the former Vice President, in a statement to CDC Gaming Reports, said he &#8220;doesn&rsquo;t support adding unnecessary restrictions to the gaming industry like the Trump Administration has done.&#8221;

The Act, from 1961, aimed to stop organised crime and the integrity of sports, by preventing a bookmaking black market and so stopped interstate betting on sports.

The Federal Wire Act has seen two interpretations in the past eight years alone, with the first coming in 2011, which Biden supports, limiting the legislation&rsquo;s restrictions to sports betting. The interpretation also allowed states to legalise and regulate online gaming.

Elsewhere, Biden issued comment on attempts by US Senators Chuck Schumer and Orrin Hatch to pass an Act that would see the federal government regulate sports betting.

Biden does not oppose it, but said he &#8220;believes states and federal authorities should cooperate to ensure gambling is safe, fair, and corruption-free.&#8221;

Biden spoke to members of Culinary Workers Local 226 in Las Vegas last week and, in a statement from the campaign, described gaming and tourism as &#8220;huge drivers of Nevada&rsquo;s economy and supporting more than 360,000 jobs.&#8221;