---
title: 'Conservative majority a    cause for celebration    for gambling industry  says consultant'
author: xforeal 
type: post
date: 2019-12-13T00:00:00+00:00
excerpt: 'Gambling consultant Steve Donoughue says the Conservative Party&rsquo;s General Election triumph should see the British gambling sector &#8220;muddle through'
url: /conservative-majority-a-cause-for-celebration-for-gambling-industry-says-consultant/
image : images/news/2019/12/BorisJohnson.jpg
categories:
  - news

---
Gambling consultant Steve Donoughue says the Conservative Party&rsquo;s General Election triumph should see the British gambling sector &#8220;muddle through.&#8221;

While Donoughue believes all MPs currently share an anti-gambling sentiment, following the fixed-odds betting terminal (FOBT) saga, he thinks the Tories&rsquo; historic victory is a preferable outcome for the industry.

While the Labour Party often championed stern anti-gambling policies, Donoughue feels the Conservative Party&rsquo;s focus on Brexit will steer it away from policing the sector too heavily.

He told _Gambling Insider_: &#8220;The fact Boris Johnson has got the biggest Tory majority since 1987 and Jeremy Corbyn has got the lowest number of Labour seats since 1935 can only be a cause for celebration for the British gambling industry.

&#8220;Not a big party with balloons and a cake but one of those office-based things with no alcohol, rubbish biscuits and everyone just dying to go home. For practically all MPs basically dislike gambling now thanks to the way the big three bookmakers amateurishly dealt with FOBTs.

&#8220;But what we now have is a Tory government focused on Brexit for the next few years and, as long as they can keep away from giving the gambling Ministership to someone susceptible to militant propaganda, we should be able to muddle through.&#8221;

A bigger positive for the sector, as far as Donoughue is concerned, is the presence of the recently formed Betting & Gaming Council.

Under the &#8220;superb&#8221; leadership of Chairperson Brigid Simmonds OBE and CEO Michael Dugher, the consultant is confident the industry won&rsquo;t be &#8220;going backwards anymore.&#8221;

&#8220;The really good news is the new Betting & Gaming Council has an absolutely superb Chair in Brigid Simmonds and a super-talented CEO in ex-Labour MP, Michael Dugher,&#8221; Donoughue explained.

&#8220;Both know Westminster intimately, both are professional lobbyists and both are super intelligent. So with an almost neutral Government and a crack team at the trade association, hopefully no more bad things will happen.

&#8220;We can&rsquo;t expect extra goodies until the industry rebuilds its reputation, of course, but hopefully we&#8217;re not going backwards anymore.&#8221;