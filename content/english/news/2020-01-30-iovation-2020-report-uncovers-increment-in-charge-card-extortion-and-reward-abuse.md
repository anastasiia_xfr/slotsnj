---
title: Iovation 2020 report uncovers increment in charge card extortion and reward abuse
author: xforeal 
type: post
date: 2020-01-30T00:00:00+00:00
excerpt: 'Mobile telephone use is as yet rising yet worries about security are still high, Iovation&amp;rsquo;s 2020 web based gaming report has revealed '
url: /iovation-2020-report-uncovers-increment-in-charge-card-extortion-and-reward-abuse/
image : images/news/2020/02/IovationLogo-2.jpg
categories:
  - news

---
Mobile telephone use is as yet rising however worries about security are still high, Iovation&rsquo;s 2020 web based gaming report has revealed.

Online gaming exchanges from cell phones have expanded 44&percnt; in the course of recent years, from 35&percnt; to 79&percnt;. Nonetheless, 63&percnt; of buyers overviewed were prevented from opening a record by means of a cell phone in view of worries about the security of their information.

Other figures legitimize consumers&rsquo; worries, with clients detailing a 37&percnt; year-on-year development in Mastercard extortion for 2019.

The report is presently in its fourth year and originates from Iovation screening in excess of four billion web based betting exchanges; Iovation has examined these for signs of misrepresentation in the course of the last 15 years.

Self-avoidance is on the ascent, expanding 63&percnt;, with 363,000 reports got last year.&nbsp;

Across the 16-page report, reward misuse was the main revealed misrepresentation by Iovation&rsquo;s clients for the third year straight, rising 72&percnt;.&nbsp;

The report additionally discovered charge card extortion has ascended by 37&percnt;. This should put the basic on more prominent security, yet must be overseen successfully, guaranteeing as meager grating as feasible for the purchaser looking to serenely utilize the product.

Greg Pierson, SVP of Business Planning and Development at Iovation&#8217;s parent organization TransUnion, stated: &#171;Providing a safe and erosion right portable experience to locally available new players has never been increasingly significant for contending viably in the web based gaming market with new nations and states apparently authorizing web based betting each week.&#187;

Discussing store rewards, Pierson included: &#171;Deposit rewards can be a significant device for pulling in and holding players.&nbsp;Unfortunately, a couple of rotten ones can manhandle in any case powerful projects to the point of dispensing with all their value.&#187;