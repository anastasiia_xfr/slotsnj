---
title: Tiger Palace lays off laborers because of budgetary difficulties
author: xforeal 
type: post
date: 2020-09-17T00:00:00+00:00
excerpt: 'Tiger Palace Resort, possessed by the Australian-recorded Silver Heritage Group, purportedly laid off 400 employees '
url: /tiger-palace-lays-off-laborers-because-of-budgetary-difficulties/
image : images/news/2020/09/silverheritagefinancial-2.jpg
categories:
  - news

---
<span data-contrast="auto">Tiger Palace Resort, claimed by the Australian-recorded Silver Heritage Group, supposedly laid off 400 workers. The report originated from The Himalayan Times after the laborers arranged a dissent over unreasonable end of agreement. </span><span data-ccp-props='{"134233117":true,"134233118":true,"201341983":0,"335559740":240}' />

<span data-contrast="auto">Reportedly, the representatives got an email from the administration, expressing their two-year contracts were ended and would not be reappointed. Monetary challenges were refered to as the principle explanation for the cutbacks. The news stories expressed: The mail was sent to all the laborers without having any conversations or gatherings with the workers. The laborers contend that such activities are against theLabourAct. Moreover, the cutbacks apparently influenced just the Nepali representatives, while unfamiliar specialists proceeded with their agreement. </span><span data-ccp-props='{"134233117":true,"134233118":true,"201341983":0,"335559740":240}' />

<span data-contrast="auto">According to Inside Asian Gaming, Silver Heritage Group expressed the agreements of the workers being referred to terminated for the current year and they have been offered a chance to sign new ones. The gathering expressed they were paying the representatives to get them through the pandemic, yet the club is anticipated to be shut until 2021. </span><span data-ccp-props='{"134233117":true,"134233118":true,"201341983":0,"335559740":240}' />

<span data-contrast="auto">Last month,HatchAsiaput forward a proposition to assume control over the gathering </span><span data-contrast="auto" /><span data-contrast="auto">and </span><span data-contrast="auto">inject AUD530m ($387m) in real money into the compan </span><span data-contrast="auto">y </span><span data-contrast="auto">, and as of late, </span><span data-contrast="auto">Silver Heritage Group executed a Deed of Company Arrangement that </span><span data-contrast="auto">would give </span><span data-contrast="auto">AU </span><span data-contrast="auto">D </span><span data-contrast="auto">530,000 ($387,000) </span><span data-contrast="auto">in money </span><span data-contrast="auto">by November </span><span data-contrast="auto">. </span><span data-ccp-props='{"134233117":true,"134233118":true,"201341983":0,"335559740":240}' />