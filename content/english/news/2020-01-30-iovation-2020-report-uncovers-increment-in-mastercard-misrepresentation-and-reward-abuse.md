---
title: Iovation 2020 report uncovers increment in Mastercard misrepresentation and reward abuse
author: xforeal 
type: post
date: 2020-01-30T00:00:00+00:00
excerpt: 'Mobile telephone utilization is as yet rising yet worries about security are still high, Iovation&amp;rsquo;s 2020 internet gaming report has revealed '
url: /iovation-2020-report-uncovers-increment-in-mastercard-misrepresentation-and-reward-abuse/
image : images/news/2020/02/IovationLogo.jpg
categories:
  - news

---
Mobile telephone utilization is as yet rising however worries about security are still high, Iovation&rsquo;s 2020 web based gaming report has revealed.

Online gaming exchanges from cell phones have expanded 44&percnt; in the course of recent years, from 35&percnt; to 79&percnt;. In any case, 63&percnt; of purchasers studied were hindered from opening a record through a cell phone due to worries about the security of their information.

Other figures legitimize consumers&rsquo; worries, with clients detailing a 37&percnt; year-on-year development in Visa misrepresentation for 2019.

The report is currently in its fourth year and originates from Iovation screening in excess of four billion internet betting exchanges; Iovation has filtered these for signs of misrepresentation in the course of the last 15 years.

Self-avoidance is on the ascent, expanding 63&percnt;, with 363,000 reports got last year.&nbsp;

Across the 16-page report, reward misuse was the main detailed extortion by Iovation&rsquo;s clients for the third year straight, rising 72&percnt;.&nbsp;

The report likewise discovered Visa misrepresentation has ascended by 37&percnt;. This should put the basic on more prominent security, yet must be overseen adequately, guaranteeing as meager grinding as feasible for the purchaser looking to easily utilize the product.

Greg Pierson, SVP of Business Planning and Development at Iovation&#8217;s parent organization TransUnion, stated: &#171;Providing a safe and contact right versatile experience to installed new players has never been progressively significant for contending successfully in the web based gaming market with new nations and states apparently legitimizing internet betting each week.&#187;

Discussing store rewards, Pierson included: &#171;Deposit rewards can be an important instrument for drawing in and holding players.&nbsp;Unfortunately, a couple of rotten ones can manhandle in any case powerful projects to the point of wiping out all their value.&#187;