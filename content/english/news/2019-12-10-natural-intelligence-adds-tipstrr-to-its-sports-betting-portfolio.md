---
title: Natural Intelligence adds Tipstrr to its sports betting portfolio
author: xforeal 
type: post
date: 2019-12-10T00:00:00+00:00
excerpt: Affiliate Natural Intelligence has announced the acquisition of sports betting tips platform Tipstrr
url: /natural-intelligence-adds-tipstrr-to-its-sports-betting-portfolio/
image : images/news/2019/12/tipstrr.jpg
categories:
  - news

---
Affiliate Natural Intelligence has announced the acquisition of sports betting tips platform Tipstrr.

Tipstrr, founded in 2014 by Damien Fearn, Andy Jones and Liam Cooper, offers sports bettors access to betting tips from verified tipsters.

Fearn said: &#8220;I&#8217;m delighted that Tipstrr is joining Natural Intelligence. It became clear very early on that the fit was perfect, and that Tipstrr and Natural Intelligence could together, offer genuine value to our users.

&ldquo;It&rsquo;s been a great journey &ndash; going from a small start-up verifying a handful of tipsters, to processing thousands of tips on a daily basis, but there&#8217;s still so much more we want to offer our users.&#8221;

Founded in 2009, Natural Intelligence acquired US-based smart odds comparison platform Sidelines earlier this year.

Jonathan Assif, Head of Business Development and Strategic Partnerships in Natural Intelligence&rsquo;s gaming business unit, said: &#8220;The acquisition is part of the company&#8217;s growth and long-term strategy of providing valuable information to empower users to make informed decisions.&rdquo;