---
title: Evolution extends South African gaming offer
author: xforeal 
type: post
date: 2020-09-16T00:00:00+00:00
excerpt: 'Evolution Services South Africa, a joint undertaking of Evolution Gaming and its auxiliary Ezugi, has expanded its organization with South African bookmaker Hollywoodbets, presently giving restrictive admittance to Ezugi Live Dealer games grew explicitly for Indian customers '
url: /evolution-extends-south-african-gaming-offer/
image : images/news/2020/09/evolutiongaming.jpg
categories:
  - news

---
Evolution Services South Africa, a joint undertaking of Evolution Gaming and its auxiliary Ezugi, has broadened its association with South African bookmaker Hollywoodbets, presently giving select admittance to Ezugi Live Dealer games grew explicitly for Indian clients. 

The set-up of games incorporates the mainstream game Andar Bahar, poker variations Teen Patti and Bet on Teen Patti, and Lucky 7, another game created for Indian players. 

The arrangement follows solid income development and is a characteristic augmentation of Hollywoodbets existing Ezugi and Evolution-fueled live vendor offering. Hollywoodbets is South Africas top wagering brand and has predominant piece of the overall industry of the neighborhood Indian populace in KwaZulu-Natal. 

Weve been met with a fabulous reaction from our online players and the solid income development created by our dispatch of Ezugi and Evolution Live Dealer games at hollywoodbets.net not long ago, said Dermot OConnell, Hollywoodbetsbetting activities director. 

&#171;The four new Indian games are being added to satisfy player need from Indian players living in South Africa, who are a significant aspect of our client base. A large number of those players are inhabitant in Durban, KwaZulu-Natal, a city near our souls as it was the place we opened our absolute first retail branch in 2000. 

Commenting on the arrangement, Evolution Services South Africa CEO Dean Finder included: Localized and culture-explicit games are a lot of part of the Ezugi offering, so we are enchanted to have the option to additionally stretch out Hollywoodbets Live Dealer offering to address the issues of explicit client gatherings.