---
title: Intralot announces new US CFO following leadership shake-up
author: xforeal 
type: post
date: 2019-12-09T00:00:00+00:00
excerpt: Intralot has announced the appointment of Lampros Klironomos as Chief Financial Officer of its US subsidiary
url: /intralot-announces-new-us-cfo-following-leadership-shake-up/
image : images/news/2019/12/INTRALOT.jpg
categories:
  - news

---
Intralot has announced the appointment of Lampros Klironomos as Chief Financial Officer of its US subsidiary.

Klironomos moves from his position as Chief Internal Audit Officer, and will assume responsibility in the group&rsquo;s US division, upon the completion of necessary legal procedures.

The group has been undergoing a leadership restructure, with Alexandra Moulavasilis elected as Director of Internal Audit, which was approved by the firm&rsquo;s Audit and Compliance Committee, following a meeting involving the Board of Directors on 3 December.

The appointment of Klironomos is part of the supplier&rsquo;s overall aim to achieve strategic objectives and improve the group&rsquo;s financial performance.

The news comes shortly after Intralot reported a 5% year-on-year decrease for its Q3 revenue, to &euro;177.5m ($196.3m).

Revenue for the first nine months of the year went down 7% to &euro;555.6m, but Intralot has since expanded its sports betting offering in the US, signing an extension with New Hampshire Lottery last month, with overall sales in the US increasing.