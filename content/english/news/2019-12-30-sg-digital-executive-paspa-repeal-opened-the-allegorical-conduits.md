---
title: SG Digital executive PASPA repeal opened the allegorical conduits
author: xforeal 
type: post
date: 2019-12-30T00:00:00+00:00
excerpt: 'Keith O&rsquo;Loughlin, SVP, Sportsbook and Platforms at SG Digital, has disclosed to Gambling Insider PASPA&rsquo;s repeal &#8220;opened the allegorical conduits&#8221; concerning sports betting'
url: /sg-digital-executive-paspa-repeal-opened-the-allegorical-conduits/
image : images/news/2019/12/scientific-games.jpg
categories:
  - news

---
<div class="mb-3">
  Keith O’Loughlin, SVP, Sportsbook and Platforms at SG Digital, has told <em>Gambling Insider</em> PASPA’s repeal “opened the allegorical conduits” concerning sports betting.&nbsp;
</div>

<div class="mb-3">
  When asked whether US dispositions towards sports wagering had changed with the cancelation of PASPA, O’Loughlin stated: “Absolutely. PASPA’s repeal opened the figurative conduits, and sports wagering dynamically has gotten progressively imbued in the discussion encompassing betting as a whole.&nbsp;
</div>

<div class="mb-3">
  “The US appears to be available to change and development in sports wagering and it is an extraordinary open door for providers and administrators to offer new and creative arrangements custom-made to the US client. The PASPA choice was the sparkle that touched off this change, and it hasn’t eased back since.”
</div>

<div class="mb-3">
  O’Loughlin additionally talked about whether an expansion of betting publicizing in US sports will adversely affect the business’ picture in the country.
</div>

<div class="mb-3">
  He stated: “It’s difficult to state, truly, yet we need to gain from recorded patterns and push ahead with past victories and hiccups similarly in mind.&nbsp;
</div>

<div class="mb-3">
  “As the US crowd structures and sports fans begin to have a superior handle on sports wagering, the industry needs to check out what’s working, what isn’t, and how bettors and non-bettors respond to an evolving landscape.”
</div>