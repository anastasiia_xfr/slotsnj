---
title: CEO Special 2020 Intralot — Part of a universal story
author: xforeal 
type: post
date: 2020-01-30T00:00:00+00:00
excerpt: 'Intralot Group Deputy CEO Chris Sfatos talks with Owain Flanders about his involved acquaintance with the organization and how he has discovered the first year in quite a while new role '
url: /ceo-special-2020-intralot-part-of-a-universal-story/
categories:
  - news

---
**Intralot Group Deputy CEO Chris Sfatos talks with Owain Flanders about his involved acquaintance with the organization and how he has discovered the first year in quite a while new role.**

If you needed to summarize Chris Sfatos in single word, it would be committed. He has given his whole profession to the Intracom/Intralot gathering, first contribution guidance on global extension and now, as Group Deputy CEO of Intralot, he is liable for procedure, money and HR. For over 23 years, Sfatos has remained as probably the sturdiest column in the group&rsquo;s establishments; helping the organization develop from humble Greek beginnings during the 1990s to turning into a globally referred to organization, going about as both administrator and provider in 47 wards over the world.

He passes on a quiet and gathered way as we talk about his encounters in the business, and probably it is this poise has helped him progress towards his present position, managing any troubles the part tosses at him en route. Sfatos has created spades of industry information, however he demands it is a Harvard University instruction in science that has outfitted him with probably the most significant apparatuses in his possession.

While experiencing childhood in Greece, Sfatos first discovered motivation in quite a while guardians, and afterward in his different educators, as he built up an enthusiasm for a wide scope of scholarly subjects, from expressions to humanities and maths. Obviously, with such a hunger for information, Sfatos decided to proceed with his investigations with a PhD course in Chemistry and Physics at Harvard, and he accepts this time of concentrate assumed a noteworthy job in the improvement of his scientific mind.

Speaking with _NJ Slots Online_, he says: &ldquo;Those years&nbsp; are developmental years in somebody&rsquo;s life, in light of the quantity of things that shape your character when you&rsquo;re youthful. At that point there are things that occur during your life that shape your mind, the manner in which you think, and the diagnostic structure. I was blessed to have awesome scientific examinations which permitted me to make an expository edge of reasoning, both quantitative and calculated. I think on the grounds that about this I was constantly engaged and attempting to see the master plan by adjusting all the details.&rdquo;

After his Harvard years, Sfatos left his dearest universe of the scholarly world and changed into the business segment by working initially for non-benefit associations, and afterward revenue driven associations, working in hazard the executives, global relations and corporate undertakings. Right now, has constantly held a craving to be a piece of a worldwide story; something he says made his current company&rsquo;s want for development so appealing.

> There are things that occur during your life that shape your mind, the manner in which you think, and the explanatory system. I was blessed to have generally excellent diagnostic investigations which permitted me to make a logical edge of thinking

Sfatos started his association with Intralot through its unique parent organization Intracom &#8212; an innovation bunch creating everything from telecoms advances to programming for the open part. It was Intracom&rsquo;s Greek base camp alongside an aspiration for worldwide strength that previously pulled in Sfatos, two perspectives that he accepts &ldquo;form the DNA of the company.&rdquo; There, he appropriated the group&rsquo;s advancements over the globe, cutting out a name for Intracom in Greece, across southwestern Europe and afterward further afield.

In 1992, Intralot was set up as a gaming innovation turn off of Intracom, and Sfatos clarifies how it felt consistent to join this organization later. He says: &ldquo;For me, it was somewhat characteristic, developing from one situation to the next, being a specialist of both change and progression any place I took part. I am appreciative to the proprietor and originator of the gathering for confiding in me for such a significant number of years.&rdquo;

In 2016, Sfatos became Group Director of Corporate Affairs before advancing to his present job as Deputy CEO, making him liable for methodology, fund and HR. In spite of the fact that he exhorted the Intracom family for a long time on global issues, Sfatos accepts his three years as Group Director of Corporate Affairs at Intralot were especially useful in causing him to comprehend the association back to front. He says: &ldquo;The work in corporate issues and managing overwhelming issues, emergency the board and the picture of the organization worldwide has given me access to many, various pieces of the organization. I was blessed to have the option to prompt the family for some years.&rdquo;

Throughout his association with the gaming business, Sfatos has encountered a total move in the gaming part, from an exclusively land-based market pre-2000 to the rise of the online space. Clarifying the difficulties and advantages of the two unique markets, he says: &ldquo;The land-based carefully managed market can bear the cost of higher payouts and offers a progressively straightforward, secure way to the players and to the more extensive open. Then again, the online channel is amazingly serious; volume and speed matter a great deal and you are truly contending on a worldwide scale.&rdquo;

<blockquote class="right">
  <p>
    The work in corporate undertakings and managing overwhelming issues, emergency the board and the picture of the organization worldwide has given me access to many, a wide range of parts of the organization. I was lucky to have the option to exhort the family for some years.&#187;
  </p>
</blockquote>

Although these improvements fundamentally adjusted the space where Intralot works, it appears to have just expanded the company&rsquo;s impact far and wide. In 2018, Intralot took care of roughly &euro;17.5bn ($19.6bn) in bets, with up to 285,000 of its exclusive terminals sent around the world, and produced absolute income of &euro;870.8m for the year.

In the most recent exchanging report accessible at the hour of composing, Intralot saw a 7&percnt; drop in income year-on-year to &euro;555.6m for the nine months to September 2019. Be that as it may, with various new agreements fixed during the time over the globe, Sfatos is centered around the advancing needs of the cutting edge buyer and the arrival of a progression of new items to oblige those players.

He says: &ldquo;We are in the start of propelling the up and coming age of items &#8212; items that are adjusted to serve the requirements of the cutting edge time and the advanced shopper. This has had some effect on our outcomes in light of the size of the venture, as a result of the difference in procedure from developing markets to develop markets like the US, which are increasingly capital-escalated, and furthermore a few changes in the market portfolio.&rdquo;

Although it probably won&#8217;t have been the least demanding introduction year for Sfatos in his job as Deputy CEO, he has seen various achievements of which he can appropriately be pleased. To modify for the speculations made in item improvement, Intralot received another structure making operational efficiencies and cost reserve funds. A year ago likewise observed the dispatch of Intralot&rsquo;s new lottery stage, Lotos X, with the company&rsquo;s individual Greek customer OPAP.

Speaking of his first year in the last place anyone would want to be, Sfatos says: &ldquo;This has been a colossally energizing period, yet in addition an exceptionally distressing one for our groups, who have invested a major energy in to make this fruitful movement for OPAP and to make new open doors for our vital customer. Therefore, this has been a significant achievement corresponding to our cost survey and the advancement of our expense bases.&rdquo;

It would be awry to make reference to Intralot&rsquo;s 2019 without thinking about its noteworthy US development, with two new games wagering contracts marked and lottery bargains broadened. At the point when the subject emerges, Sfatos ventures to characterize it as the company&rsquo;s &ldquo;main story,&rdquo; and it&rsquo;s not hard to perceive any reason why. With the toppling of PASPA, Intralot has plans in progress to build its games wagering center in the US, and associations with state lotteries in the District of Columbia, Montana, New Hampshire and New Mexico should give the administrator the ideal stage to do just that.

Discussing this new games wagering center, Sfatos clarifies he is decidedly ready for the moderate procedure of guideline the nation over, while continually watching out for venture into further states. He says: &ldquo;We feel that in a managed industry like our own, the administrative structure gets vigorous, unsurprising and settled. The development will be moderate, so we are set up for that, yet we are centered around the mass market. That will originate from the managed lottery business instead of the specialty gambling club business.

&ldquo;Our contracts with existing customers will be the primary open doors where we will dispatch our games wagering item in the US and it will be a decent springboard for the remainder of the open doors that will develop in other, greater states. Clearly greater states are an increasingly mind boggling scene with regards to making the full and stable administrative system, yet we see that developing inside the following a few years.&rdquo;

One of Intralot&rsquo;s most huge difficulties of 2019 came comparable to its no-offer games wagering contract with the state lottery in DC, and before our discussion closes, it is obviously critical to approach Sfatos for his interpretation of it. The arrangement was at the focal point of analysis from various administrators, including FanDuel and DraftKings, who both composed publications in the Washington Post censuring the understanding. In spite of the fact that the arrangement was at first blocked while a preliminary occurred, it has since been allowed to proceed after a Superior Court Judge lifted the fundamental directive. In spite of the fact that that choice doesn&#8217;t end the case, it permits the DC lottery to start executing sports wagering in its retail locales and to begin chip away at the presentation of mobile.&nbsp;

> DC is an extremely world of politics. I think it was progressively political strain that advanced this clamor and challenge, yet we confronted this. We have produced $500m of state income over the most recent 10 years for the DC state lottery and it has made huge connections of trust in us

When inquired as to why he accepts the arrangement collected such a lot of consideration, Sfatos clarifies his conviction that governmental issues had its part to play. He says: &ldquo;It is regular for a state lottery to go on with their current provider and maintain a strategic distance from the danger of relocation to another stage or another provider. The expansion to incorporate games wagering occurred in corresponding to the augmentation of the lottery contract for the following five years, so that is very natural.

&ldquo;DC is an exceptionally world of politics. I think it was progressively political pressure that advanced this commotion and challenge, however we confronted this. We have produced $500m of state income over the most recent 10 years for the DC state lottery and it has made critical connections of trust in us and our technologies.&rdquo;

With lottery contributions in 31 locales over the globe and sports wagering in 11, the universal reach of the organization is plain to see.

But for the future, Sfatos&rsquo; sights are solidly determined to giving an ideal lottery item, while guaranteeing the support of Intralot&rsquo;s standards and values.

&ldquo;In five years&#8217; time, I figure everyone will concur that we have the best lottery stage,&#187; he says. &#171;That is our fundamental desire &ndash; to have the best lottery stage on the planet. We are likewise dedicated to working with the World Lottery Association condition and everything that accompanies it, including being focused on mindful gaming and offering amusement and commitments to great motivations. That is the thing that truly characterizes our approach.&rdquo;

&nbsp;

<h2 style="line-tallness: 40px;">
  <em><strong>Read the remainder of the CEO Special underneath or <a href="#">click here</a> to get the print edition</strong></em>
</h2>

<div class="videoWrapper">
</div>