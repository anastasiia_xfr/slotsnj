---
title: IBIA report shows 31 decline in suspicious wagering alerts
author: xforeal 
type: post
date: 2020-01-29T00:00:00+00:00
excerpt: 'Suspicious wagering cautions in 2019 fell by 31&amp;percnt; year-on-year, as indicated by a report from the International Betting Integrity Association (IBIA) '
url: /ibia-report-shows-31-decline-in-suspicious-wagering-alerts/
image : images/news/2020/02/IBIA-2.jpg
categories:
  - news

---
Suspicious wagering cautions in 2019 fell by 31&percnt; year-on-year, as indicated by a report from the International Betting Integrity Association (IBIA).

The IBIA respectability report found 183 cautions were accounted for to the significant experts in 2019, 31&percnt; down on the 267 hailed in 2018.

Tennis saw the most noteworthy drop of 43&percnt;, albeit still had the most cautions with 101; alongside football, it represented 82&percnt; of all alarms in 2019, with 48&percnt; originating from Europe.

The report discovered 12 distinct games across five mainlands had alarms announced, with 52&percnt; outside Europe speaking to a 7&percnt; yearly increase.

IBIA CEO Khalid Ali stated: &#171;The decrease in cautions is exceptionally welcome, particularly as this is basically an aftereffect of an improved degree of honesty in ITF tennis, which has been the subject of specific investigation as of late. Be that as it may, there stays a reasonable danger from crooks expectation on controlling game to dupe operators.

&#171;We keep on working intimately with sports and our individuals to lessen that danger and to distinguish and rebuff such defilement, using the world&rsquo;s biggest administrator run and client information drove trustworthiness system.

&#171;Our rebranding and worldwide repositioning in 2019 has supported our development, with administrators progressively perceiving the worth and business need of taking part in aggregate activity to ensure their items against the loss of income coming about because of wagering corruption.&#187;