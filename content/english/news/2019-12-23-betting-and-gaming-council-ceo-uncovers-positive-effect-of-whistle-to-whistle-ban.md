---
title: Betting and Gaming Council CEO uncovers positive effect of whistle-to-whistle ban
author: xforeal 
type: post
date: 2019-12-23T00:00:00+00:00
excerpt: 'This summer&rsquo;s sports wagering industry-wide &lsquo;whistle-to-whistle&rsquo; publicizing boycott during live game has had a positive effect, as per the Betting and Gaming Council'
url: /betting-and-gaming-council-ceo-uncovers-positive-effect-of-whistle-to-whistle-ban/
image : images/news/2019/12/Bettingad.jpg
categories:
  - news

---
This summer’s sports wagering industry-wide ‘whistle-to-whistle’ publicizing boycott during live game has had a positive effect, as indicated by the Betting and Gaming Council.

Wes Himes, Betting and Gaming Council Interim CEO, solely told SlotsNJ the extent of complete betting business impacts in live games tumbled from 11% in 2018 to 0.65% in 2019.

Expanding on the figures, Himes stated: “This was estimated over a four-week time span in September 2018 and again the next year. That 0.6% originated from two arrangements of peculiarities; either where advertisements were contracted previously or after the whistle-to-whistle boycott, but since of programming, they ran either a couple of moments in or a couple of moments out of the watershed, or they were supplanted with other betting adverts like those for the National Lottery.”

The willful whistle-to-whistle publicizing boycott came into power in August, which implies no betting adverts can be appeared during live game – barring pony and greyhound hustling – before the 9pm watershed, enduring from five minutes before the beginning of a match and consummation five minutes after.

According to Himes, the genuine achievement of the battle is the positive effect it has had on youngsters. He included: “Likely the most significant measurement for us is the quantity of sports betting adverts seen by kids. This is four to 17-year-olds. That number tumbled from around 5,900 business impacts in 2018, right down to 203 of every 2019. So you can see the boycott has had the impact that was intended.”

The starting reaction to the boycott has been sure, with the Advertising Standards Authority (ASA) uncovering it has gotten far less betting related grievances and, up until this point, money related reports have not shown any plunge in administrator income expedited by the ban.