---
title: Spearhead Studios presents history enlivened video opening Teutoburg
author: xforeal 
type: post
date: 2020-09-15T00:00:00+00:00
excerpt: 'Spearhead Studios presents its subsequent video opening and first history-propelled title, Teutoburg '
url: /spearhead-studios-presents-history-enlivened-video-opening-teutoburg/
categories:
  - news

---
Spearhead Studios presents its subsequent video opening and first history-motivated title, Teutoburg. The new game is themed around the Battle of Teutoburg Forest, an occasion which occurred in the ninth century when a partnership of Germanic clans trapped and demolished three Roman armies. 

Specially intended to answer the desires for German players, Teutoburg is the subsequent video space of the organization, after the dispatch of Black Forest in mid-August 2020. Teutoburg has a RTP of 96&percnt;, is of medium-high unpredictability, and offers a greatest payout in abundance of 16,000x. 

Kevin Corti, Game Development Director at Spearhead Studios, says: &#171;We put a great deal of time and exertion into consummating the numerical model to guarantee that the highlights conveyed genuine energy and player esteem. Our imaginative group endeavored to make drawing in enhanced visualizations and an energizing soundscape, to praise both the game&#8217;s highlights and to fit with the recorded subject we had chosen.&#187; 

Teutoburgs key component are the secret images which uncover lucrative German or Roman mounted force, bowmen or infantry images. These secret prizes land often in the base game, and on each free turn. 

If the riddle prizes uncover enough German images, and land close to any Roman ones, a snare happens, to the enjoyment of the players. The troubled Romans are either changed over into Germans or Multiplying Wilds to make immense win potential. Free twists can retrigger and when they do, the quantity of secret prizes builds making it evident to players how tremendous successes can be possibly accomplished.