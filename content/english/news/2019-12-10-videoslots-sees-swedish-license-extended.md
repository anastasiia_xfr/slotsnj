---
title: Videoslots sees Swedish license extended
author: xforeal 
type: post
date: 2019-12-10T00:00:00+00:00
excerpt: Videoslots has had its Swedish license extended to five years by the Swedish Gaming Authority until 2023, following a successful appeal
url: /videoslots-sees-swedish-license-extended/
image : images/news/2019/12/VideoSlots.jpg
categories:
  - news

---
Videoslots has had its Swedish license extended to five years by the Swedish Gaming Authority until 2023, following a successful appeal.

The online casino had initially been given a two-year license last December, but this means it can continue to offer its portfolio of regulated offerings to players.

The operator has previously been awarded a license in Italy, Denmark, Malta, Spain and the UK..

Alexander Stevendahl, CEO at Videoslots.com, said: &ldquo;Videoslots is firmly committed to keeping gambling both safe and fun and this has been recognised with the full five-year period of our Swedish license.

&nbsp;&ldquo;Player safety across all our markets is always our priority and this news is very welcome due to the growth we are seeing in the Swedish market.&rsquo;&rsquo;

Videoslots is not the only company to have had its license extended in Sweden, with LeoVegas having had its license extended from two to five years in October.

It has been a significant 12 months for gambling in Sweden, following the re-regulation of its gambling laws at the start of the year, which has allowed private companies to apply for licenses.