---
title: Altenar CEO Setting a solid guide for 2020
author: xforeal 
type: post
date: 2020-01-30T00:00:00+00:00
excerpt: 'Stanislav Silin, Altenar CEO, talks with NJ Slots Online about the games wagering supplier&amp;rsquo;s new licenses, administrative troubles in Europe and how the organization intends to stand apart at ICE London '
url: /altenar-ceo-setting-a-solid-guide-for-2020/
image : images/news/2020/02/stanislavsilic.jpg
categories:
  - news

---
Stanislav Silin, Altenar CEO, talks with _NJ Slots Online_ about the games wagering supplier&rsquo;s new licenses, administrative troubles in Europe and how the organization intends to stand apart at ICE London.

**You&rsquo;ve now been CEO of Altenar for a long time. Disclose to us somewhat about your experience and how you originally engaged with the company.**

I am really one of the authors of the organization. I joined the organization full-time somewhat later than the others however that is on the grounds that I was recently associated with another organization. I worked for NYX, Amaya and Chartwell already &#8212; these are the gaming organizations I got a great deal of expert experience from. Initially my entrance into the gaming business originated from an extremely little English bookmaker. That is the place I found out about games wagering. Additionally, I&rsquo;m a software engineering graduate, so that&rsquo;s why I have an energy for technology.

**What have been the absolute most significant advancements in the organization in the course of the last two years?**

We&rsquo;ve achieved a decent number of administrative endorsements and licenses &#8212; two of them a year ago and one of them in 2018. As from the earliest starting point of 2018, we arranged the organization to be in a situation to get administrative endorsements. Presently we are in a situation to offer administrations to organizations, for example, Lottoland &ndash; administrators significantly greater than anything we have dealt with before.

Over the most recent two years, we likewise propelled a totally new front-end which is actually the revamp of the front-end client experience. That had a major effect in light of the fact that it&rsquo;s a responsive front-end that fits on all devices.

**What was the procedure like achieving your UK and Malta licenses a year ago? Were there any challenges?**

It was distinctive for both of them. Malta was progressively prescriptive with respect to innovation guidelines. Malta was increasingly centered around the particular foundation viewpoints. That is actually something to be thankful for on the grounds that it encourages you acquire the correct standard. The UK permit was increasingly about investigating basics &#8212; so the organization structure and who the individuals behind it are.

In a way we began with our UK permit first however we wound up getting our MGA (Malta Gaming Authority) permit before the UK permit. If we somehow managed to experience that excursion again we would switch the succession and start with the Malta permit, and afterward apply for the UK permit and acquire that in an a lot simpler fashion.

**Obviously the UK is an exceptionally serious market. Presently you have gotten your permit, what are your arrangements for having an effect on the market?**

We have a few organizations we&rsquo;re cooperated with. They give the stage administration and we give the sportsbook. There are some UK marks that are keen on getting sportsbooks so this is the means by which we plan to enter it. We haven&rsquo;t yet propelled however we&rsquo;re in planning to do that.

**Altenar is dynamic in various locales over the world. From an administrative point of view, what are the most troublesome markets for you?**

Italy and Sweden are presumably the most testing at this moment. Neither one nor the other markets expect us to hold a permit however they do expect us to cling to specific guidelines that are directed and implemented upon the administrator. The administrator in Italy is required to experience an affirmation or a test by the administrative authority legitimately. There is a quite certain convention one must regard to convey constant all the wagering data to the government.

In Sweden, it&rsquo;s not excessively the real guideline is totally different regarding getting the affirmation of endorsement, however there are sure operational necessities that make it harder -, for example, the under-18 standard. This was a test a year ago however none of our administrators got any fines.

**As we anticipate ICE one week from now, what is it about Altenar&rsquo;s offering that will make it stand apart from the crowd?**

Firstly, I trust it is a blend of various elements. We need to construct trust with our clients that we are a solid B2B provider. We don&rsquo;t have a huge turnover of clients, if any whatsoever, and typically that&rsquo;s a significant selling point.

From an innovation perspective we are setting up our new stage to be discharged in the not so distant future. This isn&rsquo;t something I can give an excess of detail on now yet it is something we are persistently taking a shot at in updating the architecture.

We are additionally extending the item portfolio in light of the fact that with a portion of the ongoing clients we marked there is a major hunger for a retail angle. There are different degrees to how a sportsbook can be actualized with retail so we are exploring different avenues regarding that in 2020.

Overall, there is a sound guide of highlights we will be dealing with this year.