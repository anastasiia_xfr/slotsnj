---
title: Betting and Gaming Council appoints new CEO
author: xforeal 
type: post
date: 2019-12-06T00:00:00+00:00
excerpt: 'Michael Dugher has been appointed the Betting and Gaming Council&rsquo;s new CEO'
url: /betting-and-gaming-council-appoints-new-ceo/
image : images/news/2019/12/boardroom3.jpg
categories:
  - news

---
Michael Dugher has been appointed the Betting and Gaming Council&rsquo;s new CEO.

The Council was formed in July, replacing the Remote Gambling Association and Association of British Bookmakers, and is chaired by Brigid Simmonds OBE.

The organisation has now found its new CEO, with Dugher due to leave his current role as CEO of UK Music in spring 2020.

Dugher has held that position since May 2017, although his previous experience came with the UK Government.

He served as MP for Barnsley East and was also the Shadow Secretary of State for Culture, Media and Sport.

The new Betting and Gaming Council CEO said: &#8220;The betting and gaming industry is a hugely significant contributor to the leisure industry, to sport and to the UK economy as a whole.

&#8220;I look forward to working with Brigid and the members of the Betting and Gaming Council as we tackle the critical issues facing the sector.

&#8220;Like many millions of people, I enjoy having a bet. Creating a safe gambling environment will be my top priority.&#8221;