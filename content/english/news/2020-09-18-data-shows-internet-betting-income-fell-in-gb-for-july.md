---
title: Data shows internet betting income fell in GB for July
author: xforeal 
type: post
date: 2020-09-18T00:00:00+00:00
excerpt: 'Gambling Commission information has uncovered the resuming of retail shops in June prompted a fall in web based betting income in Great Britain for July '
url: /data-shows-internet-betting-income-fell-in-gb-for-july/
image : images/news/2020/09/gambling-commission-logo.jpg
categories:
  - news

---
Gambling Commission information has uncovered the resuming of retail shops in June prompted a fall in web based betting income in Great Britain for July. 

Data from the greatest administrators, covering around 80&percnt; of the online market and 85&percnt; in retail in GB, looked how the Covid pandemic lockdown affected betting conduct, from March to July. 

The information indicated web based wagering gross betting yield (GGY) diminished 4&percnt; from June to July, with sports wagering GGR down 4&percnt; down to 209.3m ($271.9m) following a 115&percnt; ascent the month already. 

Virtual wagering GGR diminished for the third continuous month, down 17&percnt; month-on-month for July to 7.9m, while esports fell by 25&percnt;, to 2.6m. 

Poker GGY saw a 23&percnt; decay down to 9m after noteworthy ascent in movement during the tallness of the pandemic, while spaces just diminished by 2&percnt;, at 162.9m for July. Other gaming, including gambling club GGY, was down 4&percnt;, to 66m. 

While the Gambling Commission expressed it was the primary month-on-month fall of web based wagering GGY since April, figures were as yet an expansion on pre-pandemic figures. 

It credited the fall in online play to the resuming of retail wagering shops from 15 June, in the wake of being shut on 23 March, prompting a transaction of wagering spend. 

While the figures arent practically identical between the various periods because of the terminations, information appeared over the counter GGY at retail scenes developed to 62.5m for July, income from self help wagering terminals (SSBTs) was up to 23.2m, while GGY from machines expanded to 81.6m.