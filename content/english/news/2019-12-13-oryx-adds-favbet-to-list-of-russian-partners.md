---
title: Oryx adds Favbet to list of Russian partners
author: xforeal 
type: post
date: 2019-12-13T00:00:00+00:00
excerpt: Oryx Gaming has reached a deal to supply its content to Croatian operator Favbet
url: /oryx-adds-favbet-to-list-of-russian-partners/
image : images/news/2019/12/ORYX.jpg
categories:
  - news

---
Oryx Gaming has reached a deal to supply its content to Croatian operator Favbet.

Oryx, part of Bragg Gaming Group, will provide the bookmaker with full access to the supplier&rsquo;s RGS-supported portfolio, including titles from Gamomat and Kalamba Games.

Favbet is the third operator in Croatia that Oryx has signed with, following deals with Arena Casino and Supersport.

Matevz Mazij, Managing Director of Oryx Gaming, said: &ldquo;Croatia is fast establishing as a significant market in our expansion strategy and this deal with Favbet underlines the popularity of our portfolio with local players.&nbsp;

&ldquo;We&rsquo;re looking forward to supporting Favbet as it hits the ground running with a new online casino brand, providing a diverse selection of titles which will assist the operator in the acquisition and retention of new players.&rdquo;&nbsp;

Russian-based Favbet, a major player in land-based sports betting and casino games in Eastern Europe, is due to launch its online business at the beginning of 2020.

Head of Online at Favbet, Marko Matijevic, said: &ldquo;Oryx Gaming&rsquo;s aggregation platform offers one of the most advanced portfolios on the market and will no doubt serve us well as we expand our online offering and attain new customers.&rdquo;&nbsp;