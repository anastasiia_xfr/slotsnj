---
title: Raketech appoints new President and CEO
author: xforeal 
type: post
date: 2019-12-12T00:00:00+00:00
excerpt: 'Raketech has announced the appointment of Oskar M&uuml;hlbach&nbsp;as its new President and CEO'
url: /raketech-appoints-new-president-and-ceo/
image : images/news/2019/12/rake.jpg
categories:
  - news

---
Raketech has announced the appointment of Oskar M&uuml;hlbach&nbsp;as its new President and CEO.

M&uuml;hlbach, who has held the role of COO since April this year, will replace current CEO Michael Holmberg with immediate effect.

Christian Lundberg, Chairman of the Board at Raketech, said: &#8220;Oskar has a clear vision of how Raketech will develop towards becoming a global partner to players in the online gaming industry.

&#8220;With his excellent skills in the company&rsquo;s core business, I am confident we will ensure the right balance between organic development and expansion.

&#8220;As we now enter a new phase in the company&rsquo;s development, I am convinced he is the right person to execute on our strategy and lead the company through the challenges we and the entire industry experienced in 2019.&#8221;

In the lead-up to this change, Raketech had generated revenue of &euro;6m ($6.7m) for Q3 2019, a 15% fall year-on-year.

Adjusted EBITDA was &euro;2.7m, a drop of 34%, while profit for the period was &euro;1.2m, a decrease of 29%.

Despite this, revenue for the first nine months of 2019 saw a 1% increase, to &euro;18.1m.

The affiliate blamed the Q3 revenue drop partly on &#8220;a continued challenge in the Swedish gaming market.&#8221;