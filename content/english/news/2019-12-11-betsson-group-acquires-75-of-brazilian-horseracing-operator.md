---
title: 'Betsson Group acquires 75  of Brazilian horseracing operator'
author: xforeal 
type: post
date: 2019-12-11T00:00:00+00:00
excerpt: Betsson Group has purchased a majority stake in Brazilian licensed horseracing operator Suaposta
url: /betsson-group-acquires-75-of-brazilian-horseracing-operator/
image : images/news/2019/12/BetssonStockholm.jpg
categories:
  - news
tags:
  - Betsson Group

---
Betsson Group has purchased a majority stake in Brazilian licensed horseracing operator Suaposta.

The deal grants the Swedish operator access to the Brazilian sports betting market ahead of expected online gaming regulation in the country.

Betsson has acquired a 75% stake in Suaposta, with Founders Andre Gelfi and Fernando Correa retaining a 25% stake, remaining CEO and CFO respectively.

Gelfi said: &#8220;We are delighted to announce this agreement with Betsson Group, who are perfectly positioned to enable our business to realise its full potential in the newly regulating Brazilian market.&#8221;

The transaction was brokered by Partis and Partis Co-founder Rob Dowling said: &#8220;This deal provides an excellent outcome for both parties.

&#8220;Suaposta&rsquo;s Founders are excited to maintain an active role and to realise their significant growth plans, and Betsson Group have gained unrivalled access into the emerging LATAM online gaming market.&#8221;