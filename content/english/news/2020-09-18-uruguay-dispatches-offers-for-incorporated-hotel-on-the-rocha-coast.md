---
title: Uruguay dispatches offers for incorporated hotel on the Rocha coast
author: xforeal 
type: post
date: 2020-09-18T00:00:00+00:00
excerpt: 'The President of Uruguay LuisLacallePou, along with the Minister of Tourism Germn Cardoso, have introduced an arrangement to build up a five-star inn with a coordinated gambling club in the country '
url: /uruguay-dispatches-offers-for-incorporated-hotel-on-the-rocha-coast/
image : images/news/2020/09/uruguay.jpg
categories:
  - news

---
The President of Uruguay Luis <span data-contrast="auto">Lacalle </span><span data-contrast="auto" /><span data-contrast="auto">Pou </span><span data-contrast="auto">, along with the Minister of Tourism Germn Cardoso, have introduced an arrangement to build up a five-star inn with a coordinated gambling club in the nation. </span>

The club will be situated between the towns of La Paloma and Chu on the Rocha coast, which is regularly called the South American Riviera.<span data-ccp-props='{"134233117":true,"134233118":true,"201341983":0,"335559739":200,"335559740":240}' /> 

Cardoso said this is an exceptionally foreseen venture for the coast. He considers the to be as a chance to support Rochas advancement. We have arranged elite of conditions for the worldwide call to introduce articulations of enthusiasm for the concession and activity of a private lodging and club on the maritime bank of Rocha.<span data-ccp-props='{"134233117":true,"134233118":true,"201341983":0,"335559739":200,"335559740":240}' /> 

Cardoso included the delicate records were set up by a uniquely made group of specialists and other Ministry of Tourism authorities. The call for offers will start on September 23, with a time of 60 days for the introduction of activities.<span data-ccp-props='{"134233117":true,"134233118":true,"201341983":0,"335559739":200,"335559740":240}' /> 

Flavia Coelho, the civic chairman of Rocha,â¯said a group of experts was assembled in spring in anticipation of the call important to worldwide articulation, to determine much-wanted work in the division of Rocha, just like that fantasy of endless years with the execution of a five-star inn with a gambling club, which will give the complete drive and advancement to one of the most sublime and superb spots that the Uruguayan coast has.<span data-ccp-props='{"134233117":true,"134233118":true,"201341983":0,"335559739":200,"335559740":240}' />