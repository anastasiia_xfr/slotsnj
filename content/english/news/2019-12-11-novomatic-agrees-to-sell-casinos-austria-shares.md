---
title: Novomatic agrees to sell Casinos Austria shares
author: xforeal 
type: post
date: 2019-12-11T00:00:00+00:00
excerpt: Novomatic has agreed to sell its 17
url: /novomatic-agrees-to-sell-casinos-austria-shares/
image : images/news/2019/12/novomatic.jpg
categories:
  - news

---
Novomatic has agreed to sell its 17.19% of shares in operator Casinos Austria to lottery and sports betting operator Sazka Group.

The deal is subject to certain conditions, including regulatory approvals and rights of other Casinos Austria shareholders.

Novomatic also announced intentions to keep its 11% shares in operator &Ouml;sterreichische Lotterien..

Novomatic was originally cleared to acquire the stake in Casinos Austria two years ago.

However, the decision to sell was attributed to the ownership structure not leading to sufficient development.&nbsp;

Novomatic CEO Harald Neumann said: &ldquo;The previous ownership structure has led to no satisfactory development of Casinos Austria. As the smallest major shareholder, we have therefore decided to sell our CASAG [Casinos Austria] shares in order to allow CASAG a clear ownership structure.&nbsp;

&ldquo;It is important to enable a stable and sustainable shareholder structure that secures the company in the long term for the future challenges of national and global markets.&#8221;&nbsp;

Robert Chvatal, Sazka Group CEO, said: &nbsp;&ldquo;We believe it is the best solution for the company. We want to ensure stable positive development for CASAG in a long-term partnership of its two biggest shareholders.&#8221;