---
title: Detroit gambling clubs report 69 3m total income in August
author: xforeal 
type: post
date: 2020-09-15T00:00:00+00:00
excerpt: 'The Michigan Gaming Control Board (MGCB) declared that in August, the three Detroit gambling clubs revealed $69 '
url: /detroit-gambling-clubs-report-69-3m-total-income-in-august/
image : images/news/2020/09/michiganstate-1.jpg
categories:
  - news

---
<span data-contrast="auto">The Michigan Gaming Control Board (MGCB) declared that in August, the three Detroit gambling clubs announced $69.3 million in month to month total income. The settings were shut for over 4 months because of the COVID-19 pandemic and were allowed to resume at 15&percnt; limit. On 29 July, Governor Gretchen Whitmer permitted club to resume under severe wellbeing and security rules. </span><span data-contrast="auto">MotorCity </span><span data-contrast="auto">Casino and Greektown Casino opened its entryways on 5 August, MGM Grand Detroit returned on 7 August. </span><span data-ccp-props='{"134233117":true,"134233118":true,"201341983":0,"335559739":200,"335559740":240}' />

<span data-contrast="auto">Compared to August of the earlier year, total income for spaces and table games dropped by 42&percnt; and has declined 62&percnt; year to date against a year ago. Contrasted with August 2019, income in MGM Grand fell by 46&percnt; to $28.6m, </span><span data-contrast="auto">MotorCity </span><span data-contrast="auto">suffered a 38&percnt; decay to $25m and Greektown earned $15.7m, a 42&percnt; drop. </span><span data-ccp-props='{"134233117":true,"134233118":true,"201341983":0,"335559739":200,"335559740":240}' />

<span data-contrast="auto">Sports wagering acquired $1.97m: $932,601 for MGM, $493,275 for </span><span data-contrast="auto">MotorCity </span><span data-contrast="auto">, $551,176 for Greektown. During August, Detroit club settled $5.6m in charges, an incredible drop contrasted with $9.7m that was paid for the very month a year ago; the club additionally submitted $8.3m in betting assessments and improvement arrangement installments. </span><span data-ccp-props='{"134233117":true,"134233118":true,"201341983":0,"335559739":200,"335559740":240}' />

<span data-contrast="auto">However, the new restart </span><span data-contrast="auto">didnt </span><span data-contrast="auto">go easily. In June, Greektown let go of 600 representatives, and before the finish of September it will lay off 43 additional laborers. John Drake, VP and head supervisor of Greektown, stated: These cutbacks at Greektown Casino-Hotel are the awful consequence of COVID-19 related business conditions that were unexpected, sensational and outside our ability to control. </span><span data-ccp-props='{"134233117":true,"134233118":true,"201341983":0,"335559739":200,"335559740":240}' />

<span data-contrast="auto">MGM gathering additionally declared its arrangements to lay off near 18,000 representatives, remembering 1,100 laborers for MGM Grand Detroit club. </span><span data-ccp-props='{"134233117":true,"134233118":true,"201341983":0,"335559739":200,"335559740":240}' />

<p aria-level="2">
  <span data-ccp-props='{"201341983":0,"335559738":200,"335559739":0,"335559740":276}' />
</p>