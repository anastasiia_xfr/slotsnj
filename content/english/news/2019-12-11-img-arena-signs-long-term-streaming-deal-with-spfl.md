---
title: IMG Arena signs long-term streaming deal with SPFL
author: xforeal 
type: post
date: 2019-12-11T00:00:00+00:00
excerpt: IMG Arena has acquired the rights to Scottish Professional Football League (SPFL) content in a long-term agreement
url: /img-arena-signs-long-term-streaming-deal-with-spfl/
image : images/news/2019/12/spfl.jpg
categories:
  - news

---
IMG Arena has acquired the rights to Scottish Professional Football League (SPFL) content in a long-term agreement.

The supplier&#8217;s five-year agreement with the SPFL commences at the start of the 2020/21 season, which will see content provided to the international betting market.

This deal covers the top four tiers of Scottish football, including the Ladbrokes Premiership.

To support the partnership, IMG Arena has also signed an agreement with AI-automated sports production company Pixellot, to increase the breadth of coverage and enhance fan experience, in the first deal of its kind for the firm.

SPFL Chief Executive Neil Doncaster said: &ldquo;Deepening our mutually successful partnership with IMG Arena creates a range of increased commercial benefits, allowing us to reinvest significant revenues across the SPFL. It will also help to increase the international visibility and popularity of our competitions through innovative, best-in-class production.&rdquo;

Managing Director at IMG Arena, Freddie Longe, added: &ldquo;We are proud to renew our longstanding partnership with the SPFL. The extension of our relationship will see our remit grow further over a lengthy period and secures a key product in our portfolio. Signing this deal is a strong endorsement of our streaming product.&rdquo;