---
title: 5Dimes affirms plans to enter controlled US market
author: xforeal 
type: post
date: 2020-09-15T00:00:00+00:00
excerpt: 'Offshore sportsbook 5Dimes is seeking after endeavors to enter the controlled US market, as indicated by the widow of its founder '
url: /5dimes-affirms-plans-to-enter-controlled-us-market/
image : images/news/2020/09/Sportsbetting-1.jpg
categories:
  - news

---
Offshore sportsbook 5Dimes is seeking after endeavors to enter the managed US market, as indicated by the widow of its organizer. 

Earlier this month, the Costa Rica-based administrator declared expectations to suspend its US activities from 21 September, with a point of improving its wagering administrations. 

It indicated plans to make the most of the chance to offer an improved online games wagering experience, with the suspension expected to progress and relaunch tasks. 

Now, the widow of 5Dimes author William Sean Creighton, who was discovered dead in September 2019, affirmed the administrators intends to enter the managed market, recommending there were shocks available for its clients. 

Laura Varela stated: Sean endeavored to give the best online games wagering experience to the 5Dimes people group. I am so lowered and delighted by the numerous dependable clients who love the 5Dimes brand. 

Many of you are getting some information about the brands future. I am restricted in what I can unveil at this moment, however there are certainly some sure astonishments coming up for you. 

I am working with specialists and advisors to ensure that the brand that you have come to adore is very much spoken to in the managed U.S. market. 

Based on what Ive heard, a great deal of you need that, as well! Stay tuned, and please return again soon for refreshes!