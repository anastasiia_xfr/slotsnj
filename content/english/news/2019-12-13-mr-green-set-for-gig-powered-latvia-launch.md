---
title: Mr Green set for GiG-powered Latvia launch
author: xforeal 
type: post
date: 2019-12-13T00:00:00+00:00
excerpt: William Hill is set to launch its Mr Green brand in Latvia and has signed an agreement with Gaming Innovation Group (GiG) to use its software platform for the market entry
url: /mr-green-set-for-gig-powered-latvia-launch/
image : images/news/2019/12/william-hill-boardroom.jpg
categories:
  - news

---
William Hill is set to launch its Mr Green brand in Latvia and has signed an agreement with Gaming Innovation Group (GiG) to use its software platform for the market entry.

The long-term agreement sees GiG supply online casino, sportsbook and frontend services for Mr Green, which was purchased by William Hill earlier this year.&nbsp;

The agreement is based on revenue share and is expected to go live in Q2 2020, with a limited contribution to GiG&rsquo;s overall revenue in 2020 and an increase in 2021 onwards.

Janis Tregers, CEO of Mr Green Latvia, said: &#8220;This is an exciting moment for the William Hill Group business in Latvia, where we move forward with our strategy to introduce a global brand such as Mr Green to the market.

&ldquo;We see it as the perfect complement to the locally well-established 11.lv brand, also hosted on the GiG platform.&nbsp;

&ldquo;By working closely with GiG and leveraging their technology from product, compliance and other aspects, we have been able to propel 11.lv to second position among leading brands in the market. It is now time to make Mr Green a similar success.&#8221;

Richard Brown, GiG CEO, said: &#8220;I am pleased to announce this expansion of our collaboration and partnership with Mr Green.&nbsp;

&#8220;Latvia is an interesting market for online gambling; we are looking forward to supporting Mr Green&rsquo;s growth with a strong, safe and entertaining product.&#8221;