---
title: Rank Group shows strong development down the middle year report
author: xforeal 
type: post
date: 2020-01-30T00:00:00+00:00
excerpt: 'Rank Group developed its hidden net gaming income (NGR) by 10&amp;percnt; year-on-year for the a half year finished 31 December 2019, to &amp;pound;377 '
url: /rank-group-shows-strong-development-down-the-middle-year-report/
image : images/news/2020/02/RankGroup-1.jpg
categories:
  - news

---
Rank Group developed its fundamental net gaming income (NGR) by 10&percnt; year-on-year for the a half year finished 31 December 2019, to &pound;377.5m ($490.2m).

Underlying working benefit expanded 70&percnt; to &pound;55.1m, with the entire year sum &ndash; considering the figure for the following a half year &#8212; expected to be somewhere in the range of &pound;105m and &pound;115m.

Digital NGR went up 14&percnt; to &pound;65.2m. Grosvenor Casino&rsquo;s NGR became 15&percnt; to &pound;198.1m, yet Mecca Bingo settings demonstrated a 1&percnt; diminishing to &pound;89.6m.

The bunch procured individual administrator Stride Gaming in October for &pound;116m, which represented &pound;18.1m of NGR and &pound;1.4m of working profit.

Rank Group CEO John O&rsquo;Reilly stated: &ldquo;The income development in our computerized business and over our Grosvenor and Enracha scenes shows that we are moving the correct way in key territories of our business.&#187;