---
title: IBIA report shows 31 lessening in suspicious wagering alerts
author: xforeal 
type: post
date: 2020-01-30T17:10:16+00:00
excerpt: |
  |
    <p>Suspicious wagering alarms in 2019 fell by 31&percnt; year-on-year, as indicated by a report from the International Betting Integrity Association (IBIA) </p>
url: /ibia-report-shows-31-lessening-in-suspicious-wagering-alerts/
image : images/news/2020/01/IBIA.jpg
categories:
  - news
tags:
  - IBIA

---
Suspicious wagering alarms in 2019 fell by 31% year-on-year, as indicated by a report from the International Betting Integrity Association (IBIA).

The IBIA honesty report found 183 cautions were accounted for to the significant experts in 2019, 31% down on the 267 hailed in 2018.

Tennis saw the most critical drop of 43%, albeit still had the most alarms with 101; alongside football, it represented 82% of all cautions in 2019, with 48% originating from Europe.

The report discovered 12 unique games across five landmasses had alarms revealed, with 52% outside Europe speaking to a 7% yearly increase.

IBIA CEO Khalid Ali stated: “The decrease in alarms is exceptionally welcome, particularly as this is basically a consequence of an improved degree of uprightness in ITF tennis, which has been the subject of specific examination as of late. Notwithstanding, there stays a reasonable danger from lawbreakers purpose on controlling game to swindle operators.

“We keep on working intimately with sports and our individuals to decrease that danger and to distinguish and rebuff such defilement, using the world’s biggest administrator run and client information drove uprightness system.

“Our rebranding and worldwide repositioning in 2019 has supported our extension, with administrators progressively perceiving the worth and business need of taking part in aggregate activity to ensure their items against the loss of income coming about because of wagering corruption.”